/**
 * RegExp based Compiler for Magma Rapid Development
 * Copyright 2019 Phoenix Arts (Paweł Karaś)
 */

'use strict';

const { syntaxFilter, replaceInKeywords } = require('../addons/helpers')

let isGUI = false
let isNative = false
let natives = []

module.exports = (raw, path = '', curPath = '') => {

    let data = syntaxFilter(raw, false, true, false, true)

    let keywords = syntaxFilter(raw, true, true, false, true)





    let tree = [false]
    for (const [i, line] of data.entries()) {
        const keyLine = keywords[i]

        // autoCommas(data, keywords, i)
        // treeSyntax(data, line, keyLine, i, tree, keywords)

        getImport(data, line, keyLine, i, path)
        getExport(data, line, keyLine, i)
        getFunction(data, line, keyLine, i, tree)
        getLoop(data, line, keyLine, i)
        getTree(data, line, keyLine, i, tree)
        getClass(data, line, keyLine, i)
        getClassSetup(data, line, keyLine, i)
        getWordOperators(data, line, keyLine, i)
        getListComprehension(data, line, keyLine, i)
        varTypes(data, line, keyLine, i)
    }

    return {
        data,
        isGUI,
        isNative,
        natives
    }
}


/**
 * Replaces all import features
 */

function getImport(data, line, keyLine, i, path) {
    let simpleReg = /import/
    let complexReg = /import\b(.*)\bfrom\b(.*)/

    if (simpleReg.test(keyLine)) {
        if (complexReg.test(line)) {

            let res = complexReg.exec(line)
            data[i] = `const ${res[1]} = require(\`${path}\` + ${res[2]})`

            if (res[2] == '`electron`') isGUI = true
        }
    }
}


/**
 * Replaces all export features
 */

function getExport(data, line, keyLine, i) {
    let simpleReg = /\bexport\s+/

    if (simpleReg.test(keyLine)) {
        data[i] = line.replace(simpleReg, 'module.exports = ')
    }
}


/**
 * Converts functions
 */

function getFunction(data, line, keyLine, i, tree) {
    let simpleReg = /\bfun\b/
    let typeReg = /\bfun\s+[^\(\s]+(?=\s+[^\(]+)/
    let paramReg = /\(([^\(\)]*)\)/

    if (paramReg.test(keyLine) && simpleReg.test(keyLine)) {
        let res = paramReg.exec(line)

        let newParams = paramTypes(res[1]).join(', ')

        data[i] = line.replace(paramReg, `(${newParams})`)
        line = data[i]
    }

    if (typeReg.test(keyLine)) {
        if (tree[tree.length - 2])
            data[i] = line.replace(typeReg, '')
        else
            data[i] = line.replace(typeReg, 'function ')
    }


    else if (simpleReg.test(keyLine)) {
        if (tree[tree.length - 2])
            data[i] = line.replace(simpleReg, '')
        else
            data[i] = line.replace(simpleReg, 'function ')
    }
}


/**
 * Translates Loops to For loops and While loops
 */

function getLoop (data, line, keyLine, i) {
    let simpleReg = /^\s*loop/
    let arrayReg = /^\s*loop\s+(.*)\s+with\s+([^,\s]+)\s*(?=\{)/
    let whileReg = /^\s*loop\s+\((.*)\)/
    let rangeReg = /^\s*loop\s+([^,\s]+)\s+to\s+(.*)\s*(?=\{)/
    let rangeFromReg = /^\s*loop\s+([^,\s]+)\s+from\s+(.*)\s+to\s+(.*)\s*(?=\{)/
    let indexReg = /^\s*loop\s+(.*)\s+with\s+\(\s*(\S+)\s*,\s*(\S+)\s*\)\s*(?=\{)/

    if (whileReg.test(keyLine)) {
        let res = whileReg.exec(line)
        let compiled = `while (${res[1]})`
        data[i] = line.replace(whileReg, compiled)
    }

    else if (indexReg.test(keyLine)) {
        let res = indexReg.exec(line)
        res[3] = res[3].trim()
        let compiled = `for (let [${res[2]}, ${res[3]}] of (typeof(${res[1]}) == 'string')
    ? ${res[1]}.split('').entries()
    : (Array.isArray(${res[1]})
    ? ${res[1]}.entries()
    : Object.entries(${res[1]})))`
        data[i] = line.replace(indexReg, compiled)
    }


    else if (rangeFromReg.test(keyLine)) {
        let res = rangeFromReg.exec(line)
        let compiled = `for (let ${res[1]} = ${res[2]}; ${res[1]} < ${res[3]}; ${res[1]}++ )`
        data[i] = line.replace(rangeFromReg, compiled)
    }

    else if (rangeReg.test(keyLine)) {
        let res = rangeReg.exec(line)
        let compiled = `for (let ${res[1]} = 0; ${res[1]} < ${res[2]}; ${res[1]}++ )`
        data[i] = line.replace(rangeReg, compiled)
    }

    else if (arrayReg.test(keyLine)) {
        let res = arrayReg.exec(line)
        res[2] = res[2].trim()
        let compiled = `for (let ${res[2]} of (Array.isArray(${res[1]})
    ? ${res[1]}
    : Object.keys(${res[1]})) )`
        data[i] = line.replace(arrayReg, compiled)
    }

    else if (simpleReg.test(keyLine)) {
        data[i] = line.replace(simpleReg, 'while(true)')
    }
}


/**
 * Translates Trees into JavaScript Objects
 */

function getTree (data, line, keyLine, i, tree) {
    let closedReg = /tree\s+(\S+)\s+\{\s*\}/
    let simpleReg = /tree\s+(\S+)\s+\{/

    if (closedReg.test(keyLine)) {
        let res = closedReg.exec(line)
        let isTree = (tree.last())

        let eq = (isTree)
            ? ':'
            : '='
        let decl = (isTree)
            ? ''
            : 'const '

        let compiled = `${decl}${res[1]} ${eq} {}`
        data[i] = line.replace(closedReg, compiled)
    }

    else if (simpleReg.test(keyLine)) {
        let res = simpleReg.exec(line)


        let isTree = (tree[tree.length - 2])
        let eq = (isTree)
            ? ':'
            : '='
        let decl = (isTree)
            ? ''
            : 'const '

        let compiled = `${decl}${res[1]} ${eq} {`
        data[i] = line.replace(simpleReg, compiled)
    }
}


/**
 * Translates to JavaScript Class
 */

function getClass (data, line, keyLine, i) {
    let simpleReg = /^\s*idea\s+(\S+)/
    let extendedReg = /^\s*idea\s+(\S+)\s*of\s*(\S+)/

    if (extendedReg.test(keyLine)) {


        let res = extendedReg.exec(line)
        let compiled = `class ${res[1]} extends ${res[2]}`
        data[i] = line.replace(extendedReg, compiled)
    }

    else if (simpleReg.test(keyLine)) {
        let res = simpleReg.exec(line)
        let compiled = `class ${res[1]}`
        data[i] = line.replace(simpleReg, compiled)
    }
}


/**
 * Translates to JavaScript Constructor
 */

function getClassSetup (data, line, keyLine, i) {
    let simpleReg = /\bsetup(\b|\()/

    if (simpleReg.test(keyLine))
        data[i] = line.replace(simpleReg, 'constructor')
}


/**
 * Translate AND, OR, NOT operators
 */

function getWordOperators(data, line, keyLine, i) {
    data[i] = replaceInKeywords(data[i], '\\band\\b', '&&')
    data[i] = replaceInKeywords(data[i], '\\bor\\b', '||')
    data[i] = replaceInKeywords(data[i], '\\bnot\\b', '!')
}


/**
 * Converts List Comprehensions to ES6
 */

function getListComprehension(data, line, keyLine, i) {
    let simpleReg = /\[(.*)\s+loop\s+(\S+)\s*in\s*(.*)\]/
    let complexReg = /\[(.*)\s+loop\s+(\S+)\s*,\s*(\S+)\s*in\s*(.*)\]/

    if (simpleReg.test(keyLine)) {
        let res = simpleReg.exec(line)
        let compiled = `(function () {
    // List Comprehension
    let _$temp = []
    for (let ${res[2]} of ${res[3]})
    _$temp.push(${res[1]})
    return _$temp
})()`
        data[i] = line.replace(simpleReg, compiled)
    }

    else if (complexReg.test(keyLine)) {
        let res = complexReg.exec(line)
        let compiled = `(function () {
    // List Comprehension
    let _$temp = []
    for (let [${res[2]},${res[3]}] of (typeof(${res[4]}) == 'string')
        ? ${res[4]}.split('').entries()
        : ${res[4]}.entries())
    _$temp.push(${res[1]})
    return _$temp
})()`
data[i] = line.replace(complexReg, compiled)
}
}



/**
 * Translates contents of tree
 */

function treeSyntax(data, line, keyLine, i, tree, keywords) {
    let treeReg = /\btree\b/
    let comma = /,\s*$/
    let inc = 0
    let openings = ['{', '[', '(']
    let closings = ['}', ']', ')']
    let hiddenTree = /(\(|,|=)\s*\{/
    let trimData = data[i].trim()


    if (treeReg.test(keyLine) || hiddenTree.test(keyLine)) {
        tree.push(true)
        inc++
    }

    let keyLineArr = keyLine.split('')

    for (let [j, sym] of keyLineArr.entries()) {

        if (openings.includes(sym)) {
            if (inc) {
                inc--
            } else {
                tree.push(false)
            }
        }

        else if (closings.includes(sym)) {
            tree.pop()
        }

        else if (sym == '=') {
            if (keyLine[j + 1] == '=' || inc) {
                j++
            }

            else {
                if (tree[tree.length - 1]) {
                    let line = data[i].split('')
                    line[j] = ':'
                    data[i] = line.join('')
                }
            }
        }
    }

    // Check for Strings to not include them
    let lineArr = line.split('')

    for (const [j, sym] of lineArr.entries()) {
        if (lineArr[j - 1] != '\\' && (sym == '\'' || sym == '`')) {

            if (tree[tree.length - 1] == -1) {
                tree.pop()
            }

            else {
                tree.push(-1)
            }
        }
    }


    if (!comma.test(line) && tree.last() > 0 && trimData.length) {
        let curLine = data[i].split('')

        if (openings.includes(trimData.last())) return

        curLine.push(',')
        data[i] = curLine.join('')
    }

}

/**
 * Gets rid of types or converts them to optimized code
 */

function varTypes (data, line, keyLine, i) {
    let simpleReg = /(let|const)\s+([^\{|\s]+)\s+([^=]+)/
    let noAssignReg = /(let|const)\s+([^\{|\s]+)\s+([^=]+)$/
    // console.log(line);

    if (simpleReg.test(keyLine)) {
        let res = simpleReg.exec(line)
        let compiled = `let ${res[3]}`



        if (noAssignReg.test(keyLine)) {
            let add = ''

            if (res[2] == 'string')
                add = '``'
            else if (res[2] == 'int')
                add = '0'
            else if (res[2] == 'float')
                add = '0.0'
            else if (res[2] == 'bool')
                add = 'false'
            else if (res[2][0] == '[' && res[2].last() == ']')
                add = '[]'
            else
                add = 'new ' + res[2] + '()'

            compiled += ' = ' + add

        }

        data[i] = line.replace(simpleReg, compiled)

    }
}



/**
 * Appends native functions
 */



/**
 * Translates params
 * @param {String} params - raw format 'string name, int age'
 */

function paramTypes(params, native = false) {
    let newParams = []
    let nativeParams = []
    params = params.split(',')

    for (let i = 0; i < params.length; i++) {
        let line = params[i].trim().split(' ')
        let compiled = line.last()
        let type = []

        if (line.length == 2) {
            compiled += ' = ' + getParamType(line)
            type = [line.last(), getParamType(line, true)]
        }

        else {
            type = [line.last(), 'any']
        }

        newParams.push(compiled)
        nativeParams.push(type)

    }

    // console.log(newParams.join(', ')); //RESULT
    // console.log(nativeParams); //RESULT
    return (native
        ? nativeParams
        : newParams
    )
}


function getParamType(line, native) {
    if (native) {
        let primitives = [
            'string', 'int',
            'float', 'bool'
        ]

        if (primitives.includes(line[0])) {
            return line[0]
        }

        else if (line[0][0] == '[' && line[0].last() == ']') {
            let sliced = line[0].slice(1, -1)

            if (primitives.includes(sliced))
                return '[' + sliced + ']'
            else
                return '[' + getParamType([sliced], true) + ']'
        }

        else {
            return 'object'
        }
    }

    else {
        let add = ''

        if (line[0] == 'string')
            add = '``'
        else if (line[0] == 'int')
            add = '0'
        else if (line[0] == 'float')
            add = '0.0'
        else if (line[0] == 'bool')
            add = 'false'
        else if (line[0][0] == '[' && line[0].last() == ']')
            add = '[]'
        else
            add = 'new ' + line[0] + '()'
        return add
    }
}
