const MagmaScript = require('../main.js')

// Change this variable if you don't want to run the script
const RUN = true

let magma = new MagmaScript({
    input : __dirname + '/magma/target.mg',
    output : __dirname + '/javascript/bundle.js',
    std: true,
    bundle: true
})

if (RUN) {
    eval(magma.result)
}

