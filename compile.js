const MagmaScript = require('./main')

new MagmaScript({
    input: __dirname + '/bin/run.mg',
    output: __dirname + '/bin/out.js',
    global: true
})