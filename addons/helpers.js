module.exports = {
    clearSyntax,
    insertToArr,
    polyFills,
    syntaxFilter,
    replaceInKeywords,
    getFirstValue,
    random
}


/**
 * Get rid of some literals for syntax checking
 * @param {Array} data - Remove Strings from Lines of code
 * @param {Object} options - Which literals to remove
 */

function clearSyntax (data, options) {
    
    // String literals: [multiline, double-quoted, single-quoted]
    let strings = [false, false, false]
    let anyString = 0
    
    // Comment literals: [singleline, multiline]
    let comments = [false, false]
    let anyComment = 0
    
    let res = ['']
    
    for (const [i, rawLine] of data.entries()) {
        let line = rawLine.split('')
        res[i] = ''
        comments[0] = false
        anyComment = 0
        comments.forEach(item => anyComment += item)
        
        for (const [j, sym] of line.entries()) {
            const cur = line[j]
            const prev = line[j - 1]        
            
            if (options.comment && !anyString) {
                let comm = clearComments(cur, prev)
                if (comm[0] && !anyString) {
                    if(comm[1] == 0) {
                        res[i] = res[i].slice(0, -1)
                        comments[0] = true
                    }

                    else if (comm[1] == 1) {
                        if (comm[2]) {
                            res[i] = res[i].slice(0, -1)
                            comments[1] = true
                        } else
                            comments[1] = false
                    }

                    // Count comment literals that are toggled
                    anyComment = 0
                    comments.forEach(item => anyComment += item)
                    continue
                }
            }

            if (options.string) {
                let str = clearString(cur, prev)
                if(str[0] && !anyComment) {
                    // Toggle String Mode
                    strings[str[1]] = !strings[str[1]]

                    // Count string literals that are toggled
                    anyString = 0
                    strings.forEach(item => anyString += item)
                }
            }

            if (anyString) {
                res[i] += ' '
            }
            
            // Append to resulting array
            if (!anyString && !anyComment) {
                res[i] += sym
            }
        }
    }
    
    return res
}


function clearString(cur, prev) {
    if (prev != '\\' && cur == '`')
        return [true, 0]

    else if (prev != '\\' && cur == '"')
        return [true, 1]

    else if (prev != '\\' && cur == '\'')
        return [true, 2]
    
    else
        return [false, null]
}

function clearComments(cur, prev) {
    if (prev == '/' && cur == '/')
        return [true, 0]
    
    else if (prev == '/' && cur == '*')
        return [true, 1, true]
    
    else if (prev == '*' && cur == '/')
        return [true, 1, false]
    
    else
        return [false, null]
}

// [strings] baker([string], bool)
function syntaxFilter(data, keywords = false, bakeStrings = true, cppStrings = false, slashComments = false) {
    let tokens = []
    let compression = []

    let token = ""

    let str = false
    let comment = false
    let slashComment = false

    let braces = []
    let opened = [
        '(', '['
    ]
    let closed = [
        ')', ']'
    ]
    
    let bracketPrevent = /.*[^']*\b(import|const|let)\b[^']*(\{[^']*\})[^']*.*/

    let stringSymbol = cppStrings ? '"' : '`'
    let stringSym = cppStrings? '`' : '\''

    let compressed = 0
    
    
    for (let line of data) {

        compressed++

        for (let i = 0; i < line.length; i++) {
            let sym = line[i]
            let prev = (i) ? line[i - 1] : i
            
            
            // Comments '/**/'
            if (! str) {
                if (prev == '*' && sym == '/') {
                    comment = false
                    if (token.length) token = token.slice(0, -1)
                    continue
                }
                
                if (comment) {
                    continue
                }
                
                if (prev == '/' && sym == '*') {
                    comment = true
                    if (token.length) token = token.slice(0, -1)
                    continue
                }
            }

            // Slash Comments '//'
            if (!slashComments) {
                if (!str && !comment && !slashComment) {
                    if (sym == '/' && prev == '/') {
                        if (token.length) token = token.slice(0, -1)
                        slashComment = true
                        token += ' '
                        break
                    }
                }
            }
            
            
            // Strings            
            if (sym == stringSym && prev != '\\') {
                if (!comment && !slashComment)
                    token += (bakeStrings) ? stringSymbol : '\''
                
                str = !str
                continue
            }

            
            // Separate ';'
            if (sym == ';' && !str && !comment) {
                if (token.trim().length) {
                    tokens.push(token)
                    token = ''
                    continue
                }

                else {
                    token = ''
                    continue
                }
            }
            
            // Separate '{'
            if (sym == '{' && !str && !comment) {
                
                if (!bracketPrevent.test(line)) {
                    token += '{'
                    tokens.push(token)
                    token = ''
                    braces = []
                    continue
                }
            }
            
            // Brace Line Continuers
            if (opened.includes(sym)) {                
                braces.push(sym)
            }
            
            else if (closed.includes(sym)) {
                if (braces.length && opened[closed.indexOf(sym)] == braces.last()) {
                    braces.pop()
                }
            }
            
            // Default
            if (!comment && !slashComment) {
                if (str && keywords) { 
                    token += ' '
                    continue
                }
                
                token += sym
            }
            
        }
        
        slashComment = false
        
        if (token.length || !line.length) {
            if (str) {
                token += (keywords) ? ' ' : '\\n'
                compressed++
            }
            
            else if (!braces.length) {                
                tokens.push(token)
                compression.push(compressed)
                compressed = 0
                token = ''
            }

            else compressed++
        }
    }
    
    return tokens
}




function replaceInKeywords(line = '', what = '', to = '') {
    let reg = new RegExp(what, 'g')
    let splits = line.split('`')

    for (let i = 0; i < splits.length; i += 2)
        splits[i] = splits[i].replace(reg, to)
    
    return splits.join('`')
}



function insertToArr (array, index, toBeInserted) {
    for (let i = 0; i < array.length; i++) {
        if (i == index) {
            array[i] = toBeInserted[0]
            toBeInserted = toBeInserted.reverse()

            for (let j = 0; j < toBeInserted.length - 1; j++) {
                array.splice(index + 1, 0, toBeInserted[j])
            }
        }
    }

    return array
}

function polyFills () {
    
    // Array Last Item Polyfill
    Array.prototype.last = function () {
        return this[this.length - 1]
    }

    // String Last Item Polyfill
    String.prototype.last = function () {
        return this[this.length - 1]
    }

    String.prototype.firstUpper = function () {
        return this[0].toUpperCase() + this.substring(1)
    }
}


function getFirstValue (line) {
    let res = ''
    let after = ''
    let notSeparatorReg = /[^+-=*,;/%]/
    let openings = [ '(', '[' ]
    let closings = [ ')', ']' ]
    let scope = 0
    let str = false
    let prev = null
    let end = false

    line = line.trim()
    
    for (let sym of line) {

        if (!end) {
            if (openings.includes(sym)) scope++
            if (closings.includes(sym)) scope--
            if (sym == '\'' && prev != '\\') str = !str
    
            prev = sym
    
    
            if (notSeparatorReg.test(sym) || scope || str) {
                res += sym
            }
    
            else {
                end = true
            }
        }

        else {
            after += sym
        }

        
    }
    
    return [res, after]
}


function random(from = 0, to = 1) {
    if (to === undefined) {
        return Math.random() * from
    }

    return (Math.random() * (to - from)) + from
}