const File = (() => {
    const fs = require(`` + `fs`)

    class File {
        constructor(path) {
            this.path = path
        }

        read(encoding = `utf-8`) {
            return fs.readFileSync(this.path, encoding)
        }

    }

    return File
})()