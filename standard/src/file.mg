import fs from 'fs'

> File {
    setup(path) {
        this.path = path
    }

    read(encoding = 'utf-8') {
        return fs.readFileSync(this.path, encoding)
    }

}


export File