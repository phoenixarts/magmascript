#!/usr/bin/env node

/**
 * Standard Library For MagmaScript Language
 * Copyright 2019 - Phoenix Arts (Paweł Karaś)
 */

function log(...content) {
    console.log(...content)
}

function random(min = 0, max = 1) {
    return (Math.random() * max) + min
}

function sqrt(number) {
    return Math.sqrt(number)
}

Array.prototype.last = function() {
    return this[this.length - 1]
}

String.prototype.lower = function() {
    return this.toLowerCase()
}

String.prototype.upper = function() {
    return this.toUpperCase()
}

String.prototype.firstLower = function() {
    return this[0].toLowerCase() + this.substring(1)
}

String.prototype.firstUpper = function() {
    return this[0].toUpperCase() + this.substring(1)
}

if (typeof(document) != 'undefined') {
    function element(query) {
        return document.querySelector(query)
    }

    function elements(query) {
        return new Array(...document.querySelectorAll(query))
    }

    HTMLElement.prototype.css = function(key, val) {
        let newKey = ''

        for (let i = 0; i < key.length; i++) {
            if (key[i] == '-')
                newKey += key[++i].toUpperCase()
            else
                newKey += key[i]
        }

        this.style[newKey] = val
    }

    HTMLElement.prototype.magmaEvents = {}

    HTMLElement.prototype.on = function(event, callback, options = false) {
        let stateList = this.magmaEvents[event]

        this.magmaEvents[event] = (stateList == null) ? [] : stateList
        this.magmaEvents[event].push([callback, options])

        this.addEventListener(event, callback, options)

        return (this.magmaEvents[event].length - 1)
    }

    HTMLElement.prototype.detach = function(event, id = null) {
        if (isNaN(id)) {
            for (let index = 0; index < this.magmaEvents[event].length; index++) {
                this.removeEventListener(event, ...this.magmaEvents[event][index])
            }
        } else {
            this.removeEventListener(event, ...this.magmaEvents[event][id])
        }
    }

    HTMLElement.prototype.parent = function(up = 1) {
        let el = this

        for (; up > 0; up--) {
            el = el.parentElement
        }

        return el
    }
}

/**
 * The Actual Code
 */

'use strict';

const path = require(`path`)
const chalk = require(`chalk`)
const MagmaScript = require(path.join(__dirname, `../main.js`))

let args = process.argv

let help = chalk.yellow(`---MagmaCompiler---\n`) +
    `Usage: magma-script ` + chalk.cyan(`<file input> <file output>`) + `
       mg ` + chalk.cyan(`<file input> <file output>`)


if (args[2] == `help`) {
    log(help)
    process.exit()
} else {
    if (!args[2]) {
        log(chalk.red(`Specify input file. Run \'mg help\' for more information`))
        process.exit()
    }

    let out = (args[3]) ? args[3] : `out.js`

    new MagmaScript({
        input: path.join(process.cwd(), args[2]),
        output: path.join(process.cwd(), out),
    })
}